//
//  ViewController.swift
//  todoey
//
//  Created by Jacki Moore on 6/18/18.
//  Copyright © 2018 Jacki Moore. All rights reserved.
//

import UIKit

class TodolistViewController: UITableViewController {
    
    var itemArray = [""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return itemArray.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToDoItemCell", for: indexPath)
        cell.textLabel?.text = itemArray[indexPath.row]
        return cell
    }
    // MARK: -  Table VIew delagate meth
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        tableView.deselectRow(at: indexPath, animated: true)
        if tableView.cellForRow(at: indexPath)?.accessoryType == .checkmark
        {
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        }else
        {
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        }
        
    }
    // MARK: -  Addbuttonpressed
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        var Textfield = UITextField()
        let alert = UIAlertController(title: "Add new todoey item", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "Add Item", style: .default) { (action) in
            //WHat will happen when the user clicks the add item in the UI aleart
            if Textfield.text != ""
            {
            self.itemArray.append(Textfield.text!)
            self.tableView.reloadData()
            }
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Create new Item"
            Textfield = alertTextField
        }
    }
    
}

